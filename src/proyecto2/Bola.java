package proyecto2;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author dinat
 */
public class Bola {

    private int radio;
    private Color color;
    private int x;
    private int y;
    private int speedX;
    private int speedY;

    public Bola() {
        this.speedX = 0;
        this.speedY = 0;
    }

    public Bola(int radio, Color color, int x, int y) {
        this.radio = radio;
        this.color = color;
        this.x = x;
        this.y = y;
        this.speedX = 5;
        this.speedY = 5;
    }
    
    /**
     * Pinta y arma la bola
     * @param g variable del Graphics
     */
       
    public void paint(Graphics g) {

        g.setColor(color);
        g.fillOval(x, y, radio, radio);
    }

    /**
     * Crea un límite en la ventana y así los objetos no sobrepasen las medidas.
     * @return 
     */
    public Rectangle getBounds() {

        Rectangle temp = new Rectangle(x, y, radio, radio);
        return temp.getBounds();
    }

    public int getRadio() {
        return radio;
    }

    public void setRadio(int radio) {
        this.radio = radio;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getSpeedX() {
        return speedX;
    }

    public void setSpeedX(int speedX) {
        this.speedX = speedX;
    }

    public int getSpeedY() {
        return speedY;
    }

    public void setSpeedY(int speedY) {
        this.speedY = speedY;
    }

}
