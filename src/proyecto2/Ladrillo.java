package proyecto2;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;

/**
 *
 * @author dinat
 */
public class Ladrillo {

    private int lado;
    private Color color;
    private boolean relleno;
    private int x;
    private int y;
    private int puntos;
    private int golpes;

    public Ladrillo() {
    }

    public Ladrillo(int lado, Color color, boolean relleno, int x, int y) {
        this.lado = lado;
        this.color = color;
        this.relleno = relleno;
        this.x = x;
        this.y = y;
    }

    public double getArea() {
        return Math.pow(lado, 2);
    }

    /**
     * Crea el ladrillo y le da las medidas
     *
     * @param g
     */
    public void paint(Graphics g) {
        g.setColor(color);
        if (relleno) {
            g.fillRect(x, y, lado, lado);
        } else {
            g.drawRect(x, y, lado, lado);
        }
    }

    /**
     * Dependiendo de los golpes que reciba la nave, obtiene los puntos
     *
     * @param g
     */
    public void obtenerPuntos(Graphics g) {
        if (golpes == 3) {
            g.setColor(Color.GRAY);
            g.fillRect(puntos + 500, puntos, puntos, puntos);
        } else if (golpes == 2) {
            g.setColor(Color.BLUE);
            g.fillRect(puntos + 300, puntos, puntos, puntos);
        } else if (golpes == 1) {
            g.setColor(Color.DARK_GRAY);
            g.fillRect(puntos + 100, puntos, puntos, puntos);
        }
    }

    public int getLado() {
        return lado;
    }

    public void setLado(int lado) {
        this.lado = lado;
    }

    public boolean isRelleno() {
        return relleno;
    }

    public void setRelleno(boolean relleno) {
        this.relleno = relleno;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public int getGolpes() {
        return golpes;
    }

    public void setGolpes(int golpes) {
        this.golpes = golpes;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Ladrillo{" + "lado=" + lado + ", color=" + color + ", relleno=" + relleno + ", x=" + x + ", y=" + y + ", puntos=" + puntos + ", golpes=" + golpes + '}';
    }

}
