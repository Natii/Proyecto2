package proyecto2;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;

/**
 *
 * @author dinat
 */
public class Logica {

    private Vaus vaus;
    private Bola bola;
    private Ladrillo ladrillo1;
    private int velocidad;
    private int mover;
    private boolean moverBola;
    private boolean pause;
//    private LinkedList<Ladrillo> bloque;
    
    private Ladrillo[][] bloque;

    private String[][] nivel = {
        {"-", "-", "-", "-", "-", "-", "-", "-", "-", "-"},
        {"-", "-", "-", "-", "-", "-", "-", "-", "-", "-"},
        {"-", "-", "-", "-", "-", "-", "-", "-", "-", "-"},
        {"-", "-", "-", "-", "-", "-", "-", "-", "-", "-"},
        {"-", "-", "-", "-", "-", "-", "-", "-", "-", "-"},
        {"-", "-", "-", "-", "-", "-", "-", "-", "-", "-"},
        {"-", "-", "-", "-", "-", "-", "-", "-", "-", "-"},
        {"-", "-", "-", "-", "-", "-", "-", "-", "-", "-"},
        {"-", "-", "-", "-", "-", "-", "-", "-", "-", "-"},
        {"-", "-", "-", "-", "-", "-", "-", "-", "-", "-"}};

//        {" ", " ", " ", " ", " ", " ", " ", " "},
//        
//        {" ", " ", " ", " ", " ", " ", " ", " "},
//        
//        {" ", " ", " ", " ", " ", " ", " ", " "},
//        
//        {" ", " ", " ", " ", " ", " ", " ", " "},
//        
//        {" ", " ", " ", " ", " ", " ", " ", " "},
//        
//        {" ", " ", " ", " ", " ", " ", " ", " "}
    public Logica() {
        //               b,  h,              x,    y,  v   
        vaus = new Vaus(120, 20, Color.CYAN, 330, 630, 5);
        bola = new Bola(35, Color.PINK, 370, 595);
        ladrillo1 = new Ladrillo(30, Color.WHITE, true, 10, 20);
        bloque = new Ladrillo[10][10];
        velocidad = 10;
    }

//    /**
//     * Recorre un for que contiene los niveles y dependiendo de que nivel esté
//     * le hace un mapa de ladrillos diferente
//     *
//     * @param g
//     */
    public void paintLadrillos(Graphics g) {

        int y = 30;
        for (int f = 0; f < nivel.length; f++) {
            int x = 30;
            for (int c = 0; c < nivel[f].length; c++) {
                if (nivel[f][c].equals("-")) {
                    bloque[f][c] = new Ladrillo(30, Color.WHITE, true, 10, 20);
                    bloque[f][c].paint(g);
                }
                x += 100;
            }
            y += 50;
        }
    }

    /**
     * Si el juego no está pausado, hace el movimiento de la nave y depende la
     * dirección aumenta o decrece la velocidad
     */
    public void moverVaus() {
        if (!pause) {
            if (mover == 1) {
                vaus.setX(vaus.getX() + velocidad);
            } else if (mover == 2) {
                vaus.setX(vaus.getX() - velocidad);
            }
        }
    }

    /**
     * Mueve la bola y dependiendo donde rebote entonces toma otra dirección
     */
    public void moverBola() {
        if (moverBola && !pause) {
            if (bola.getBounds().intersects(vaus.getBounds(1))) {
                bola.setSpeedY(Math.abs(bola.getSpeedY()) * -1); //No deberia rebotar en el borde de abajo
                bola.setSpeedX(Math.abs(bola.getSpeedX()) * -1);
            } else if (bola.getBounds().intersects(vaus.getBounds(2))) {
                bola.setSpeedY(Math.abs(bola.getSpeedY()) * -1); //No deberia rebotar en el borde de abajo
            } else if (bola.getBounds().intersects(vaus.getBounds(3))) {
                bola.setSpeedY(Math.abs(bola.getSpeedY()) * -1); //No deberia rebotar en el borde de abajo
                bola.setSpeedX(Math.abs(bola.getSpeedX()));

            }

            if (bola.getX() > (785 - bola.getRadio()) && bola.getSpeedX() > 0) {
                System.out.println("Rebotar");
                bola.setSpeedX(bola.getSpeedX() * -1);
            } else if (bola.getX() < 0 && bola.getSpeedX() < 0) {
                System.out.println("Rebotar");
                bola.setSpeedX(bola.getSpeedX() * -1);
            } else {
                bola.setX(bola.getX() + bola.getSpeedX());
            }

            if (bola.getY() > (660 - bola.getRadio()) && bola.getSpeedY() > 0) {
                System.out.println("Perder vida y reiniciar la bola");
                moverBola = false;
                bola.setSpeedY(bola.getSpeedY() * -1); //No deberia rebotar en el borde de abajo
            } else if (bola.getY() < 0 && bola.getSpeedY() < 0) {
                System.out.println("Rebotar");
                bola.setSpeedY(bola.getSpeedY() * -1);
            } else {
                bola.setY(bola.getY() + bola.getSpeedY());
            }
            //bola.setY(bola.getY() + bola.getSpeedY());
        }
    }

    public void crearNiveles() {

    }

    public Vaus getVaus() {
        return vaus;
    }

    public void setVaus(Vaus vaus) {
        this.vaus = vaus;
    }

    public Bola getBola() {
        return bola;
    }

    public void setBola(Bola bola) {
        this.bola = bola;
    }

    public int getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(int velocidad) {
        this.velocidad = velocidad;
    }

    public int getMover() {
        return mover;
    }

    public void setMover(int mover) {
        this.mover = mover;
    }

    public void setMoverBola(boolean b) {
        moverBola = b;
    }

    public boolean isMoverBola() {
        return moverBola;
    }

    public boolean isPause() {
        return pause;
    }

    public void setPause(boolean pause) {
        this.pause = pause;
    }

}
