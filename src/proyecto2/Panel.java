package proyecto2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author dinat
 */
public class Panel extends JPanel implements KeyListener {

    private Logica logica;

    public Panel() {

        setPreferredSize(new Dimension(785, 660));
        addKeyListener(this);
        setFocusable(true);
        logica = new Logica();

    }

    @Override
    /**
     * Pinta el fondo y los demás objetos
     */
    public void paint(Graphics g) {

        super.paint(g);
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, getWidth(), getHeight());
        logica.getVaus().setTipo(1);
        logica.getVaus().paint(g);
        
        logica.paintLadrillos(g);
//        logica.getVaus().paintLaser(g);
        logica.moverBola();
        logica.getBola().paint(g);

    }

    @Override
    public void keyTyped(KeyEvent pe) {

    }

    @Override
    
    /**
     * Método de eventos que hace que se muevan los objetos
     */
    
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            if (logica.getVaus().getX() >= 0) {
                logica.setMover(2);
                logica.moverVaus();
            }
        } else if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            if (logica.getVaus().getX() <= 650) {
                logica.setMover(1);
                logica.moverVaus();
            }
        } else if (ke.getKeyCode() == KeyEvent.VK_R) {
            logica.getVaus().setX(350);
            logica.getVaus().setY(630);
            logica.getBola().setX(390);
        } else if (ke.getKeyCode() == KeyEvent.VK_P) {
            logica.setPause(!logica.isPause());
        } else if (ke.getKeyCode() == KeyEvent.VK_SPACE) {
            logica.setMoverBola(true);
        }

    }

    @Override
    public void keyReleased(KeyEvent ke) {

    }

}
