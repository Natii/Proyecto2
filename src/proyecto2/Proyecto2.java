package proyecto2;

import javax.swing.JFrame;

/**
 *
 * @author dinat
 */
public class Proyecto2 {

    public static void main(String[] args) throws InterruptedException {

        JFrame frame = new JFrame("Arkanoid");
        frame.add(new Panel());
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        while (true) {
            frame.repaint();
            Thread.sleep(20);
        }
    }

}
