package proyecto2;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author dinat
 */
public class Vaus {

    private int base;
    private int altura;
    private Color color;
    private int x;
    private int y;
    private int tipo;
    private int vidas;

    public Vaus() {

    }

    public Vaus(int base, int altura, Color color, int x, int y, int vidas) {
        this.base = base;
        this.altura = altura;
        this.color = color;
        this.x = x;
        this.y = y;
        this.vidas = vidas;
    }

    /**
     * Pinta y arma la nave y le cambia el color dependiendo el tipo de poder que tenga.
     * @param g 
     */
    
    
    public void paint(Graphics g) {
//        if (tipo == 0) {
//            g.setColor(Color.RED);
//            g.fillRect(x, y, base, altura);
            if (tipo == 1) { //Azul, se expande
                g.setColor(Color.YELLOW);
                g.fillRect(x, y, base / 4, altura);

                g.setColor(Color.BLUE);
                g.fillRect(x + base / 4, y, base / 2, altura);

                g.setColor(Color.YELLOW);
                g.fillRect(x + base / 4 * 3, y, base / 4, altura);

            } else if (tipo == 2) { //Rojo, cañón láser por 10 seg.
                g.setColor(Color.RED);
                g.fillRect(x, y, base, altura);
            } else if (tipo == 3) { //Plomo, aumenta vidas
                g.setColor(Color.DARK_GRAY);
                g.fillRect(x, y, base, altura);
                vidas += 1;
            } else if (tipo == 4) { //Verde, atrapa la pelota hasta que presione la barra
                g.setColor(Color.GREEN);
                g.fillRect(x, y, base, altura);
            } else if (tipo == 5) { // Naranja, hace más lenta la bola por 10 seg.
                g.setColor(Color.ORANGE);
                g.fillRect(x, y, base, altura);
            }
        }
    
    /**
     * Pinta y arma el cañón láser
     * @param g 
     */

    public void paintLaser(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(200, 630, 20, 20);
        g.fillRect(260, 630, 20, 20);
    }
    
    /**
     * Crea un límite en la ventana para que las figuras no sobrepasen ese límite
     * @param posicion
     * @return null por cuestión de otro caso diferente a los otros 3
     */

    public Rectangle getBounds(int posicion) {
        switch (posicion) {
            case 1:
                return new Rectangle(x, y, 30, 15);
            case 2:
                return new Rectangle(x + 30, y, 30, 15);
            case 3:
                return new Rectangle(x + 60, y, 30, 15);
            default:
                return null;
        }
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getVidas() {
        return vidas;
    }

    public void setVidas(int vidas) {
        this.vidas = vidas;
    }

}
